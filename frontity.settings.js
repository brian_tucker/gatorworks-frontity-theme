const settings = {
  "name": "gatorworks-frontity-theme",
  "state": {
    "frontity": {
      "url": "https://test.frontity.org",
      "title": "Test Frontity Blog",
      "description": "WordPress installation for Frontity development"
    }
  },
  "packages": [
  "@aamodtgroup/frontity-gravity-forms",
    {
      name: "gatorworks-frontity-theme"
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          "url": "http://gatorworks-frontity-theme.local/",
          // this links to the prod Wordpress install
          "url": "https://gatorworksfron.wpengine.com/",
          "homepage": "/home",
          "postsPage": "/news",
          "postTypes": [
            {
              type: "destinations",
              endpoint: "destinations",
              archive: "/destinations"
            },
            {
              type: "cakes",
              endpoint: "cakes",
              archive: "/cakes"
            }
          ],
          taxonomies: [
            {
              taxonomy: "regions", // taxonomy slug
              endpoint: "regions", // REST API endpoint
              postTypeEndpoint: "destinations", // endpoint from which posts from this taxonomy are fetched
            },
            {
              taxonomy: "flavors", // taxonomy slug
              endpoint: "flavors", // REST API endpoint
              postTypeEndpoint: "cakes", // endpoint from which posts from this taxonomy are fetched
            }
          ],
          gfAuth: {
            key: "ck_218868a6c17c8bd07d82f9615cb25c55f6c6677d",
            secret: "cs_5a3a7b0f417e1142cea06d1fb58913ce076b75df"
          }
        }
      }
    },
    "@frontity/tiny-router",
    "@frontity/html2react"
  ]
};

export default settings;
