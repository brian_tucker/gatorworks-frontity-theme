// File: /packages/my-first-theme/src/index.js

import Root from "./components"

import link from "@frontity/html2react/processors/link";
import menuHandler from "./components/handlers/menu-handler";
import acfHandler from "./components/handlers/acf-handler";

const gatorworksFrontityTheme = {
  name: "gatorworks-frontity-theme",
  roots: {
    theme: Root,
  },
  state: {
    theme: {
      isUrlVisible: false,
      menuUrl: 'primary-nav',
    },
  },
  actions: {
    theme: {
      toggleUrl: ({ state }) =>{
        state.theme.isUrlVisible = !state.theme.isUrlVisible
      },
      beforeSSR: async ({state, actions, libraries}) => {
        await Promise.all([
            actions.source.fetch(`/menu/${state.theme.menuUrl}/`),
            actions.source.fetch("@acf/options")
        ]);
      }
    },
  },
  libraries: {
    html2react: {
      processors: [link]
    },
    source: {
      handlers: [menuHandler, acfHandler],
    },
  },
};

export default gatorworksFrontityTheme