// File: /packages/my-first-theme/src/components/post.js

import React from "react"
import { connect, styled, Head } from "frontity"
import dayjs from "dayjs"

const Post = ({ state, libraries }) => {
  const data = state.source.get(state.router.link)
  const post = state.source[data.type][data.id]
  const author = state.source.author[post.author]
  const formattedDate = dayjs(post.date).format("DD MMMM YYYY")
  const Html2React = libraries.html2react.Component;

  const featuredImgId = post.featured_media;

  if(featuredImgId){
    var featuredImg = state.source.attachment[featuredImgId].source_url;
  }

  if(data.isHome){
    if(data.isPage){
      return (
        <div>
          <Head>
            <title>{post.title.rendered}</title>
            <meta name="description" content="{post.excerpt.rendered}" ></meta>
          </Head>
          
          <HomeContent isPostType={data.isPostType} isPage={data.isPage}>
            <Html2React html={post.content.rendered} />
          </HomeContent>
        </div>
      )
    }
  }
  else{

  

    if(data.isPage){
      return (
        <div>
          <Head>
            <title>{post.title.rendered}</title>
            <meta name="description" content="{post.excerpt.rendered}" ></meta>
          </Head>
          <PostContent isPostType={data.isPostType} isPage={data.isPage}>
            <Html2React html={post.content.rendered} />
          </PostContent>
        </div>
      )
    }

    if(data.isPost){
      return (
        <div>
          <img src={featuredImg}></img>
          <Head>
            <title>{post.title.rendered}</title>
            <meta name="description" content="{post.excerpt.rendered}" ></meta>
          </Head>
          <h2>{post.title.rendered}</h2>
          <PostInfo>
            <p>
              <strong>Posted: </strong>
              {formattedDate}
            </p>
            <p>
              <strong>Author: </strong>
              {author.name}
            </p>
          </PostInfo>
          <PostContent>
            <Html2React html={post.content.rendered} />
          </PostContent>
        </div>
      )
    }
    else{
      return (
        <div>
          <Head>
            <title>{post.title.rendered}</title>
            <meta name="description" content="{post.excerpt.rendered}" ></meta>
          </Head>
          <h2>{post.title.rendered}</h2>
          <PostContent>
            <Html2React html={post.content.rendered} />
          </PostContent>
        </div>
      )
    }

  }
  
}

export default connect(Post)

const PostInfo = styled.div`
  background-image: linear-gradient(to right, #f4f4f4, #fff);
  margin-bottom: 1em;
  padding: 0.5em;
  border-left: 4px solid lightseagreen;
  font-size: 0.8em;

  & > p {
    margin: 0;
  }
`

const PostContent = styled.div`
  /*background: #f5e9e2;*/
  padding: 60px 40px;

  width: 100vw;
  margin-left: calc(50% - 50vw);

  display: flex;
  flex-direction: column;
  align-items: center;

  & > p{
    max-width: 650px;
    text-align: center;
  }

  & > .wp-block-image{
    margin-top: 30px;
    margin-bottom: 40px;
  }

  //this can be the general styling for any cpt that comes through...
  & > .cpt-block{
    /*background: #ded;*/
    padding: 10px;

    display: flex;
    flex-direction: column;
  }
`

const HomeContent = styled.div`

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-self: center;

  & > .wp-block-image {
    margin-bottom: 40px;
    margin-top: 30px;
  }

  & > .hero-main{
    height: 70vh;

    margin: 1em 0;
    margin-top: 0;
    width: 100vw;
    margin-bottom: 60px;

    & > :before{
        content: '';

        height: 100%;
        width: 100%;

        background: #f5e9e2;

        position: absolute;
        z-index: 6;

        opacity: 0.8;
      }

    & > .hero-inner{
      position: relative;
      height: 100%;
      width: 100%; 

      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;

      padding: 0 30px;

      & > .img-cont{
        position: absolute;
        height: 100%;
        width: 100%;
        z-index: 5;

        & > img{
          height: 100%;
          width: 100%;
          object-fit: cover;
        }

      }

      & > .content-cont{
        background: #559cad;
        color: #fff;
        height: -moz-max-content;
        height: fit-content;
        width: -moz-max-content;
        width: fit-content;
        z-index: 7;
        padding: 30px 60px;
        position: relative;
        
        align-items: center;
        border-radius: 10px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        text-align: center;

        & > h1{
          margin-bottom: 10px;
          /*text-transform: uppercase;*/
        }

        & > p{
          max-width: 600px;
        }

      }
    }
  }
`