// File: /packages/my-first-theme/src/components/index.js
import React from "react"
import Error from "./error"
import { connect, Global, css, styled, Head } from "frontity"
import Link from "@frontity/components/link"
import Switch from "@frontity/components/switch"
import Loading from "./loading"
import List from "./list"
import Post from "./post"
import Header from "./header"
import Destination from "./destination"
import Footer from "./footer"


const Root = ({ state, actions }) => {
	const data = state.source.get(state.router.link)

  // Get the fetched data stored in state.
//   const optionsData = state.source.get("@acf/options");
// 
//   if(optionsData.isAcfOptions){
//     //these are the options for the site. Need to import them on any component that they want to be used for.
//     console.log(optionsData.items.acf);
//   }


  if(data.isHome){
    const post = state.source[data.type][data.id]

    return (
    <>
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Arvo:wght@400;700&display=swap" rel="stylesheet" />
        <title>Gatorworks Theme</title>
        <meta
            name="description"
            content="Based on the Frontity step by step tutorial"
          />
      </Head>
      <Global
        styles={css`
        *{
          margin: 0;
          padding: 0;
          box-sizing: border-box;       
        }
          html {
            font-family: Arvo, system-ui, Verdana, Arial, sans-serif;
          }
        `}
      />

      <Header />

      <Main>
        <Switch>
          <Loading when={data.isFetching} />
          <Post when={data.isPage} />
          <Error when={data.isError} />
        </Switch>
      </Main>

      <Footer />

    </>
    )
  }
  else{
    return (
    <>
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Arvo:wght@400;700&display=swap" rel="stylesheet" />
        <title>Gatorworks Theme</title>
        <meta
            name="description"
            content="Based on the Frontity step by step tutorial"
          />
      </Head>
      <Global
        styles={css`
        *{
          margin: 0;
          padding: 0;
          box-sizing: border-box;       
        }
          html {
            font-family: Arvo, system-ui, Verdana, Arial, sans-serif;
          }
        `}
      />

      <Header />

      <Main>
        <Switch>
          <Loading when={data.isFetching} />
          <List when={data.isArchive} />
          <Post when={data.isPost} />
          <Post when={data.isPage} />
          <Destination when={data.isDestinations} />
          <Error when={data.isError} />
        </Switch>
      </Main>

      <Footer />

    </>
    )
  }
}


export default connect(Root)

const Main = styled.main`
  max-width: 1230px;
  margin: auto;
  padding: 0 30px;
  padding-top: 0;

  img {
    max-width: 100%;
  }
  h2 {
    margin: 0.5em 0;
    margin-top: 0;
  }
  p {
    line-height: 1.25em;
    margin-bottom: 10px;
  }
  figcaption {
    color: #828282;
    font-size: 0.8em;
    margin-bottom: 1em;
  }
`
