// File: /packages/my-first-theme/src/components/list.js
import React from "react"
import { connect, styled } from "frontity"
import Link from "@frontity/components/link"

const List = ({ state, actions, libraries }) => {
  const data = state.source.get(state.router.link)
  const Html2React = libraries.html2react.Component

  const regions = state.source.regions;
  const flavors = state.source.flavors;

  var newLinkArr;

  // TODO use isPostArchive to figure out if its the news archive or not

  if(data.isCakesArchive){
    // this is the array of the flavors. We can now grab the ids
    var newArrayFlavors = Object.values(flavors)
    newLinkArr = [];
    var tmpStr;

    // this is a list of the regions taxonomy, you could do a little reverse engineering to figure out how to create other taxonomies
    for(var x = 0; x < newArrayFlavors.length; x++){
      tmpStr = "<li><a href='\\" + newArrayFlavors[x].slug +  "'>" + newArrayFlavors[x].name + "</a></li>"; 
      newLinkArr.push(tmpStr);
    }
  }

  if(data.isDestinationsArchive){ 
    //this is the array of the regions. We can now grab the ids
    var newArrayRegions = Object.values(regions)
    newLinkArr = [];
    var tmpStr;

    // this is a list of the regions taxonomy, you could do a little reverse engineering to figure out how to create other taxonomies
    for(var x = 0; x < newArrayRegions.length; x++){
      tmpStr = "<li><a href='\\" + newArrayRegions[x].slug +  "'>" + newArrayRegions[x].name + "</a></li>"; 
      newLinkArr.push(tmpStr);
    }
  }

  //rn, they are seperated bescause of the taxononmy regions cat filter...the news will also have a filter
  //but it may not be for the same taxonomy...so keeping it seperate might be good...for now
  if(data.isDestinationsArchive){
    return(
      
      <Items>
        <Menu>

          <span>Regions:</span>
          {newArrayRegions.map((mi) => {
            
            var preSlug = "\\regions\\";

            return (
              <Link key={mi.id} link={preSlug+mi.slug} >{mi.name}</Link> 
            )
          })}   

        </Menu>
        
        {data.items.map((item) => {
          const post = state.source[item.type][item.id]

          const featuredImgId = post.featured_media;

          if(featuredImgId){
            var featuredImg = state.source.attachment[featuredImgId].source_url;
          }

          if(data.isDestinationsArchive){ 
            return (
              <MainContent key={item.id} isPostType={data.isPostType} isPage={data.isPage}>
                <h3>{post.title.rendered}</h3>
                
                  <ImgCont>
                    <img src={featuredImg}></img>
                  </ImgCont>
                  <Link key={item.id} link={post.link}>
                    Read More
                  </Link>
                
              </MainContent>
            )
          }

        })}

        <PrevNextNav>
          {data.previous && (
            <button
              onClick={() => {
                actions.router.set(data.previous)
              }}
            >
              &#171; Prev
            </button>
          )}
          {data.next && (
            <button
              onClick={() => {
                actions.router.set(data.next)
              }}
            >
              Next &#187;
            </button>
          )}
        </PrevNextNav>

      </Items>
    )
  }
  else if(data.isCakesArchive){
    return(
      
      <Items>
        <Menu>

          <span>Flavors:</span>
          {newArrayFlavors.map((mi) => {
            
            var preSlug = "\\flavors\\";

            return (
              <Link key={mi.id} link={preSlug+mi.slug} >{mi.name}</Link> 
            )
          })}   

        </Menu>
        
        {data.items.map((item) => {
          const post = state.source[item.type][item.id]

          const featuredImgId = post.featured_media;

          if(featuredImgId){
            var featuredImg = state.source.attachment[featuredImgId].source_url;
          }

          if(data.isCakesArchive){ 
            return (
              <MainContent key={item.id} isPostType={data.isPostType} isPage={data.isPage}>
                <h3>{post.title.rendered}</h3>
                
                  <ImgCont>
                    <img src={featuredImg}></img>
                  </ImgCont>
                  <Link key={item.id} link={post.link}>
                    Read More
                  </Link>
                
              </MainContent>
            )
          }

        })}

        <PrevNextNav>
          {data.previous && (
            <button
              onClick={() => {
                actions.router.set(data.previous)
              }}
            >
              &#171; Prev
            </button>
          )}
          {data.next && (
            <button
              onClick={() => {
                actions.router.set(data.next)
              }}
            >
              Next &#187;
            </button>
          )}
        </PrevNextNav>

      </Items>
    )
  }
  else{
    return(
      <Items>
        
        {data.items.map((item) => {
          const post = state.source[item.type][item.id]

          const featuredImgId = post.featured_media;

          if(featuredImgId){
            var featuredImg = state.source.attachment[featuredImgId].source_url;
          }

          if(data.isRegions || data.isFlavors){
            return (
              <MainContent key={item.id} isPostType={data.isPostType} isPage={data.isPage}>
                <h3>{post.title.rendered}</h3>
                
                  <ImgCont>
                    <img src={featuredImg}></img>
                  </ImgCont>
                  
                  <Link key={item.id} link={post.link}>
                    Read More
                  </Link>
                
              </MainContent>
            )
          }
          else{
              // creating the excerpt if it isn't a cpt
              const excerpt = post.excerpt.rendered  

              const featuredImgId = post.featured_media;

              if(featuredImgId){
                var featuredImg = state.source.attachment[featuredImgId].source_url;
              }

              return (
                <MainContent key={item.id} isPostType={data.isPostType} isPage={data.isPage}>
                  <h3>{post.title.rendered}</h3>

                  <ImgCont>
                    <img src={featuredImg}></img>
                  </ImgCont>
                  
                  <Html2React html={excerpt} />
                  <Link key={item.id} link={post.link}>
                    Read More
                  </Link>
                  
                </MainContent>


              )
            }
          

        })}

        <PrevNextNav>
          {data.previous && (
            <button
              onClick={() => {
                actions.router.set(data.previous)
              }}
            >
              &#171; Prev
            </button>
          )}
          {data.next && (
            <button
              onClick={() => {
                actions.router.set(data.next)
              }}
            >
              Next &#187;
            </button>
          )}
        </PrevNextNav>

      </Items>
    )
  }
}

export default connect(List)

const Items = styled.div`

  //example of how i would handl responsive design. 
  @media only screen and (max-width: 600px) {
    /*background: #ded;*/
  }


  & > div > h3 {
    font-size: 25px;
    margin-bottom: 0.2em;
  } 

  & > div > div > p {
    margin-bottom: 0.5em;
  }

  & > div > div > a {
    color: steelblue;
    font-weight: 600;
    display: block;
    margin-bottom: 1em;
    text-decoration: none;

    width: fit-content;
    width: -moz-max-content;

  }
`

const ImgCont = styled.div`
  width: 50%;
  height: 350px;

  margin-bottom: 20px;

  & > img{
    height: 100%;
    width: 100%;

    object-fit: cover;
  }
`

const PrevNextNav = styled.div`
  padding-top: 1.5em;

  & > button {
    background: #eee;
    text-decoration: none;
    padding: 0.5em 1em;
    color: #888;
    border: 1px solid #aaa;
    font-size: 0.8em;
    margin-right: 2em;
  }
  & > button:hover {
    cursor: pointer;
  }
`

const MainContent = styled.div`
  background: #f5e9e2;
  padding: 20px 0;
  

  border: 2px solid #773344;
  border-radius: 10px;

  border-color: ${ props => props.isPostType ? ( props.isPage ? '#559cad' : 'lightseagreen' ) : '#773344'};

  display: flex;
  flex-direction: column;
  align-items: center;

  margin-top: 40px;

  & > a{
    color: #559cad;
    text-decoration: none;
  }
`

const Menu = styled.nav`
  background: #f5e9e2;
  border-radius: 5px;
  display: flex;
  flex-direction: row;
  font-weight: 600;
  margin-top: 1em;
  /*margin-bottom: 40px;*/

  padding: 1em;
  
  & > a {
    margin-left: 1em;
    color: #559cad;
    text-decoration: none;
  }
`