export const acfHandler = {
    name: "acf-options",
    priority: 10,
    pattern: "@acf/options",
    func: async ({ link, params, state, libraries }) => {
      const { api } = libraries.source;
      const { slug } = params;
  
      // 1. fetch   the data you want from the endpoint page
      const response = await api.get({
        endpoint: `/acf/v3/options/options`,
      });
  
      // 2. get an array with each item in json format
      const items = await response.json();
  
      // 3. add data to source
      const currentPageData = state.source.data[link];
      Object.assign(currentPageData, {
        items: items,
        isAcfOptions: true,
      });
    },
  };
  
  export default acfHandler;