// File: /packages/my-first-theme/src/components/footer.js

import React from "react"
import { connect, Head, styled } from "frontity"
import Link from "@frontity/components/link"

const Footer = ({ state, libraries, actions }) => {
  const data = state.source.get(state.router.link)
  // Get the fetched data stored in state.
  const optionsData = state.source.get("@acf/options");
  const acfData = optionsData.items.acf;
  const Html2React = libraries.html2react.Component;

  //the vars from the options.....could we potentially automate the creation of these?
  var address_line_1, address_line_2, phone, phoneLink, email, emailLink, website, websiteTitle, websiteLink;

  //if the options data exists, add value to those vars
  if(optionsData.isAcfOptions){
    //these are the options for the site. Need to import them on any component that they want to be used for.
    address_line_1 = acfData.address_line_1;
    address_line_2 = acfData.address_line_2;
    phone = acfData.phone;
    //this is how you have to build the link to make calls
    phoneLink = "tel:" + phone;
    email = acfData.email;
    emailLink = "mailto:" + email;
    website = acfData.website;
    websiteTitle = website.title;
    websiteLink = website.url;
  }

  return (
    <FooterMain isPostType={data.isPostType} isPage={data.isPage}>
      <h1>Footer</h1>        
      <FooterContent>
        
        <div>
          <h5>Location</h5>
          <p>{address_line_1}</p>
          <p>{address_line_2}</p>
          <a>{email}</a>
        </div>
        <div>
        <h5>Contact Us</h5>
          <Link link={phoneLink}>{phone}</Link>
          <Link link={emailLink}>{email}</Link>
          <Link link={websiteLink}>{websiteTitle}</Link>
        </div>
      </FooterContent>
    </FooterMain>
  )
}

export default connect(Footer)


const FooterMain = styled.div`
  background-color: #f5e9e2;
  border-width: 8px 0 0 0;
  border-style: solid;
  border-color: ${ props => props.isPostType ? ( props.isPage ? '#559cad' : 'lightseagreen' ) : '#773344'};
  margin-top: 60px;

  min-height: 200px;
  padding-top: 20px;

  h1 {
    color: #4a4a4a;
    max-width: 1200px;
    margin: auto;
  }
`

const FooterContent = styled.div`
  max-width: 1200px;
  padding-bottom: 2em;
  padding-top: 1em;
  margin: auto;
  display: flex;
  justify-content: space-between;

  div{
    display: flex;
    flex-direction: column;
  }

  h5{
    font-size: 20px;
    margin-bottom: 5px;
  }

  a, p{
    margin-bottom: 5px;
  }

  a{
    color: #559cad;
  }
`

const Button = styled.button`
  background: transparent;
  border: none;
  color: #aaa;

  :hover {
    cursor: pointer;
    color: #888;
  }
`