// File: /packages/my-first-theme/src/components/header.js
import React from "react"
import Link from "@frontity/components/link"
import { connect, Head, styled } from "frontity"

const Header = ({ state, actions }) => {
  const data = state.source.get(state.router.link)
  const items = state.source.get(`/menu/${state.theme.menuUrl}/`).items; 


  /**
 * Two level menu (with one level of child menus)
 */
  return (
    <NavContainer>
    <NavContent>
    <h1>Bread's Bakery</h1>
      <NavList>
        {items.map((item) => {
          if (!item.child_items) {
            return (
              <NavItem key={item.ID}>
                <Link link={item.url}>{item.title}</Link>
              </NavItem>
            );
          } else {
            const childItems = item.child_items;
            return (
              <NavItemWithChild key={item.ID}>
                <NavItem>
                  <Link link={item.url}>{item.title}</Link>
                </NavItem>
                <ChildMenu>
                  {childItems.map((childItem) => {
                    return (
                      <NavItem key={childItem.ID}>
                        <Link link={childItem.url}>{childItem.title}</Link>
                      </NavItem>
                    );
                  })}
                </ChildMenu>
              </NavItemWithChild>
            );
          }
        })}
      </NavList>

      </NavContent>
    </NavContainer>
  );

  
}

export default connect(Header)

const NavContainer = styled.nav`

  background-color: #f5e9e2;
  border-width: 0 0 8px 0;
  border-style: solid;
  //this is an example of using the dynamic styling to determine what to change the border to
  border-color: ${ props => props.isPostType ? ( props.isPage ? '#559cad' : 'lightseagreen' ) : '#773344'};

  list-style: none;
  display: flex;
  justify-content: center;
  width: 848px;
  /*max-width: 100%;*/
  box-sizing: border-box;
  padding: 30px 24px;
  margin: 0;
  /* overflow-x: auto; */
  overflow: hidden;

  width: 100%;

  @media screen and (max-width: 560px) {
    display: none;
  }
`;

const NavContent = styled.div`
  display: flex;
  /*background: #dad;*/
  flex-direction: column;
  max-width: 1230px;
  width: 100%;

  & > h1{
    color: #4a4a4a;
    font-size: 2em;
    margin-bottom: 10px;
  }
`

const NavList = styled.div`
  /*background: #ded;*/
  display: flex;

  width: fit-content;
  width: -moz-max-content;

  & > a {
    
  }
`

const NavItem = styled.div`
  padding: 0;
  margin: 0 16px;
  color: #fff;
  /*font-size: 0.9em;*/
  box-sizing: border-box;
  flex-shrink: 0;
  & > a {
    display: inline-block;
    line-height: 2em;
    color: #559cad;
    text-decoration: none;

    /* Use for semantic approach to style the current link */
    &[aria-current="page"] {
      border-bottom-color: #fff;
    }
  }
  &:first-of-type {
    margin-left: 0;
  }
  &:last-of-type {
    margin-right: 0;
    &:after {
      content: "";
      display: inline-block;
      width: 24px;
    }
  }
`;

/**
 * Styling of nav elements
 */
const NavItemWithChild = styled.div`
  background: pink;
`;
const ChildMenu = styled.div`
  left: 0;
  background-color: lightblue;
  width: 100%;
  z-index: 1;
`;